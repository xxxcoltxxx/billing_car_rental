<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    id
 * @property string title
 */
class CarClass extends Model
{
    protected $fillable = [
        'title',
    ];

    public $timestamps = false;
}
