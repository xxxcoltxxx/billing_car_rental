<?php

namespace App\Models\Billing;

use App\Models\CarClass;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int                       billing_plan_service_id
 * @property int                       billing_condition_id
 * @property int                       billing_plan_modification_id
 * @property int                       car_class_id
 * @property string                    title
 * @property float                     modification_amount
 * @property array                     condition_parameters
 * @property-read  BillingCondition    billingCondition
 * @property-read  BillingModification billingModification
 * @property-read  CarClass|null       carClass
 */
class BillingPlanServiceRule extends Model
{
    protected $fillable = [
        'billing_plan_service_id',
        'billing_condition_id',
        'billing_plan_modification_id',
        'car_class_id',
        'title',
        'modification_amount',
        'condition_parameters',
    ];

    protected $casts = [
        'modification_amount'  => 'float',
        'condition_parameters' => 'array',
    ];

    public function billingCondition()
    {
        return $this->belongsTo(BillingCondition::class);
    }

    public function billingModification()
    {
        return $this->belongsTo(BillingModification::class);
    }

    public function carClass()
    {
        return $this->belongsTo(CarClass::class, 'car_class_id', 'id', 'car_classes');
    }
}
