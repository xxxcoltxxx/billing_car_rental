<?php

namespace App\Models\Billing;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string title
 */
class BillingModification extends Model
{
    protected $fillable = [
        'title',
    ];

    public $timestamps = false;

    const MODIFICATION_PERCENT = 1;

    const MODIFICATION_VALUE = 2;

    const MODIFICATION_MAXIMUM = 3;
}
