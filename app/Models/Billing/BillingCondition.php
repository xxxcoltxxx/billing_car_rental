<?php

namespace App\Models\Billing;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string title
 */
class BillingCondition extends Model
{
    protected $fillable = [
        'title',
    ];

    public $timestamps = false;

    const CONDITION_LESS = 1;

    const CONDITION_GREATER = 2;

    const CONDITION_TIME = 3;
}
