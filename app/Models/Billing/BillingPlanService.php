<?php

namespace App\Models\Billing;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property int                                      id
 * @property int                                      billing_plan_id
 * @property int                                      billing_service_stage_id
 * @property float                                    price
 * @property-read BillingServiceStage                 billingServiceStage
 * @property-read BillingPlanServiceRule[]|Collection billingPlanServiceRules
 */
class BillingPlanService extends Model
{
    protected $fillable = [
        'billing_plan_id',
        'billing_service_stage_id',
        'price',
    ];

    public $timestamps = false;

    protected $casts = [
        'price' => 'float',
    ];

    public function billingServiceStage()
    {
        return $this->belongsTo(BillingServiceStage::class);
    }

    public function billingPlanServiceRules()
    {
        return $this->hasMany(BillingPlanServiceRule::class);
    }
}
