<?php

namespace App\Models\Billing;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    id
 * @property string title
 */
class BillingService extends Model
{
    protected $fillable = [
        'title',
    ];

    public $timestamps = false;

    const SERVICE_DURATION = 1;

    const SERVICE_DISTANCE = 2;
}
