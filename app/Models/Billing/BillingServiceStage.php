<?php

namespace App\Models\Billing;

use App\Models\Rent\RentStage;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int                 id
 * @property int                 rent_stage_id
 * @property int                 billing_service_id
 * @property-read BillingService billingService
 * @property-read RentStage      rentStage
 */
class BillingServiceStage extends Model
{
    protected $fillable = [
        'rent_stage_id',
        'billing_service_id',
    ];

    public $timestamps = false;

    const SERVICE_STAGE_BOOK_DURATION = 1;

    const SERVICE_STAGE_CHECK_DURATION = 2;

    const SERVICE_STAGE_DRIVE_DURATION = 3;

    const SERVICE_STAGE_DRIVE_DISTANCE = 4;

    const SERVICE_STAGE_PARKING_DURATION = 5;

    public function billingService()
    {
        return $this->belongsTo(BillingService::class);
    }

    public function rentStage()
    {
        return $this->belongsTo(RentStage::class);
    }
}
