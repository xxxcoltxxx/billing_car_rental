<?php

namespace App\Models\Billing;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int         id
 * @property string      title
 * @property Carbon      active_from
 * @property Carbon|null active_to
 */
class BillingPlan extends Model
{
    protected $fillable = [
        'title',
        'active_from',
        'active_to',
    ];

    protected $dates = [
        'active_from',
        'active_to',
    ];

    public function billingPlanServices()
    {
        return $this->hasMany(BillingPlanService::class);
    }
}
