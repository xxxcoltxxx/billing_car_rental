<?php

namespace App\Models\Rent;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    id
 * @property string title
 */
class RentStage extends Model
{
    protected $fillable = [
        'title',
    ];

    public $timestamps = false;

    const STAGE_BOOK = 1;

    const STAGE_CHECK = 2;

    const STAGE_DRIVE = 3;

    const STAGE_PARKING = 4;
}
