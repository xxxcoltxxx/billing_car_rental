<?php

namespace App\Http\Resources;

use App\Models\Billing\BillingPlan;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin BillingPlan
 */
class BillingPlanFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'title'       => $this->title,
            'active_from' => isoDateTime($this->active_from),
            'active_to'   => isoDateTime($this->active_to),
            'services'    => BillingPlanServiceFullResource::collection($this->whenLoaded('billingPlanServices')),
        ];
    }
}
