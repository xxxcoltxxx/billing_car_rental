<?php

namespace App\Http\Resources;

use App\Models\Billing\BillingCondition;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin BillingCondition
 */
class BillingConditionBriefResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'title' => $this->title,
        ];
    }
}
