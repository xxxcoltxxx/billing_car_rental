<?php

namespace App\Http\Resources;

use App\Models\Billing\BillingPlanService;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin BillingPlanService
 */
class BillingPlanServiceFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'price'           => $this->price,
            'rent_stage'      => $this->billingServiceStage->rentStage->title,
            'billing_service' => $this->billingServiceStage->billingService->title,
            'rules'           => BillingPlanServiceRulesFullResource::collection($this->whenLoaded('billingPlanServiceRules')),
        ];
    }
}
