<?php

namespace App\Http\Resources;

use App\Models\Billing\BillingModification;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin BillingModification
 */
class BillingModificationBriefResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'title' => $this->title,
        ];
    }
}
