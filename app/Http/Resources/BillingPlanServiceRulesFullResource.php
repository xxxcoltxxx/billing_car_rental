<?php

namespace App\Http\Resources;

use App\Models\Billing\BillingPlanServiceRule;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin BillingPlanServiceRule
 */
class BillingPlanServiceRulesFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title'        => $this->title,
            'car_class'    => new CarClassBriefResource($this->carClass),
            'condition'    => array_merge(
                BillingConditionBriefResource::make($this->whenLoaded('billingCondition'))->toArray($request),
                ['params' => $this->condition_parameters]
            ),
            'modification' => array_merge(
                BillingModificationBriefResource::make($this->whenLoaded('billingModification'))->toArray($request),
                ['value' => $this->modification_amount]
            ),
        ];
    }
}
