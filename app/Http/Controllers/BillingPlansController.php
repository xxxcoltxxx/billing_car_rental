<?php

namespace App\Http\Controllers;

use App\Http\Resources\BillingPlanFullResource;
use App\Models\Billing\BillingPlan;

class BillingPlansController extends Controller
{
    public function show(BillingPlan $billingPlan)
    {
        $billingPlan->loadMissing([
            'billingPlanServices.billingServiceStage.billingService',
            'billingPlanServices.billingServiceStage.rentStage',
            'billingPlanServices.billingPlanServiceRules.billingCondition',
            'billingPlanServices.billingPlanServiceRules.billingModification',
            'billingPlanServices.billingPlanServiceRules.carClass',
        ]);

        return new BillingPlanFullResource($billingPlan);
    }
}
