<?php

/**
 * Форматирует дату в Iso8601
 *
 * @param Carbon|string $date
 *
 * @return null|string
 * @throws Exception
 */
function isoDateTime($date)
{
    if (! $date) {
        return null;
    }

    if (is_string($date)) {
        $date = Carbon::parse($date);
    }

    if ($date instanceof Carbon) {
        return $date->toIso8601String();
    }

    if ($date instanceof DateTime) {
        return $date->format('c');
    }

    throw new Exception('Incorrect datetime format');
}
