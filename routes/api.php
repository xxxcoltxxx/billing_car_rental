<?php

Route::resource('billing-plans', 'BillingPlansController')
    ->only('show')
    ->names('billing-plans');
