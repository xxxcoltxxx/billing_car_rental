<?php

namespace Tests\Feature;

use App\Models\Billing\BillingPlan;
use App\Models\Billing\BillingPlanService;
use App\Models\Billing\BillingPlanServiceRule;
use App\Models\Rent\RentStage;
use Illuminate\Support\Collection;
use Tests\TestCase;

class BillingPlanTest extends TestCase
{
    public function test_return_format()
    {
        /** @var BillingPlan $plan */
        $plan = factory(BillingPlan::class)->create();
        /** @var BillingPlanService[]|Collection $services */
        $services = collect();
        $stages = [
            RentStage::STAGE_BOOK,
            RentStage::STAGE_CHECK,
            RentStage::STAGE_DRIVE,
            RentStage::STAGE_PARKING,
        ];
        foreach ($stages as $stageId) {
            $services->push(factory(BillingPlanService::class)->create([
                'billing_plan_id'          => $plan->id,
                'billing_service_stage_id' => $stageId,
            ]));
        }

        $response = $this->getJson(route('billing-plans.show', $plan));

        $response->assertSuccessful();
        $expect = [
            'id' => $plan->id,
            'title' => $plan->title,
            'active_from' => $plan->active_from->toIso8601String(),
            'active_to' => $plan->active_to,
            'services' => $services->map(function (BillingPlanService $service) {
                return [
                    'price' => $service->price,
                    'rent_stage' => $service->billingServiceStage->rentStage->title,
                    'billing_service' => $service->billingServiceStage->billingService->title,
                    'rules' => $service->billingPlanServiceRules->map(function (BillingPlanServiceRule $rule) {
                        return [
                            'title' => $rule->title,
                            'car_class' => $rule->carClass->only('id', 'title'),
                            'condition' => $rule->billingCondition->only('id', 'title'),
                            'modification' => $rule->billingModification->only('id', 'title'),
                        ];
                    })->toArray(),
                ];
            })->toArray(),
        ];

        $response->assertJson(['data' => $expect]);
    }
}
