<?php

use App\Models\Billing\BillingPlan;
use App\Models\Billing\BillingPlanService;
use App\Models\Billing\BillingServiceStage;
use Faker\Generator as Faker;

$factory->define(BillingPlanService::class, function (Faker $faker) {
    return [
        'price'                    => $faker->randomFloat(2, 0, 100),
        'billing_plan_id'          => factory(BillingPlan::class),
        'billing_service_stage_id' => $faker->randomElement([
            BillingServiceStage::SERVICE_STAGE_BOOK_DURATION,
            BillingServiceStage::SERVICE_STAGE_CHECK_DURATION,
            BillingServiceStage::SERVICE_STAGE_DRIVE_DURATION,
            BillingServiceStage::SERVICE_STAGE_DRIVE_DISTANCE,
            BillingServiceStage::SERVICE_STAGE_PARKING_DURATION,
        ]),
    ];
});
