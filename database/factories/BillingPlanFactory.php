<?php

use App\Models\Billing\BillingPlan;
use Faker\Generator as Faker;

$factory->define(BillingPlan::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'active_from' => today()->toDateTimeString(),
    ];
});
