<?php

use App\Models\CarClass;
use Faker\Generator as Faker;

$factory->define(CarClass::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
    ];
});
