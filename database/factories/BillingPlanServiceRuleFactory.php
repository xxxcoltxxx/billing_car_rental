<?php

use App\Models\Billing\BillingCondition;
use App\Models\Billing\BillingModification;
use App\Models\Billing\BillingPlanService;
use App\Models\Billing\BillingPlanServiceRule;
use App\Models\CarClass;
use Faker\Generator as Faker;

$factory->define(BillingPlanServiceRule::class, function (Faker $faker) {
    $conditionId = $faker->randomElement([
        BillingCondition::CONDITION_LESS,
        BillingCondition::CONDITION_GREATER,
        BillingCondition::CONDITION_TIME,
    ]);

    $conditionParams = $conditionId === BillingCondition::CONDITION_TIME
        ? ['from' => '00:00:00', 'to' => '07:00:00']
        : ['value' => $faker->randomNumber()];

    return [
        'billing_plan_service_id' => factory(BillingPlanService::class),
        'billing_condition_id' => $conditionId,
        'billing_modification_id' => $faker->randomElement([
            BillingModification::MODIFICATION_PERCENT,
            BillingModification::MODIFICATION_VALUE,
            BillingModification::MODIFICATION_MAXIMUM,
        ]),
        'car_class_id' => factory(CarClass::class),
        'title' => $faker->title,
        'modification_amount' => $faker->randomNumber(),
        'condition_parameters' => $conditionParams,
    ];
});
