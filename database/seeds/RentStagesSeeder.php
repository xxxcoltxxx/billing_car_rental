<?php

use App\Models\Rent\RentStage;
use Illuminate\Database\Seeder;

class RentStagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RentStage::query()
            ->updateOrCreate(['id' => RentStage::STAGE_BOOK], [
                'title' => 'Бронирование',
            ])
            ->updateOrCreate(['id' => RentStage::STAGE_CHECK], [
                'title' => 'Осмотр',
            ])
            ->updateOrCreate(['id' => RentStage::STAGE_DRIVE], [
                'title' => 'Поездка',
            ])
            ->updateOrCreate(['id' => RentStage::STAGE_PARKING], [
                'title' => 'Парковка (Ожидание)',
            ]);
    }
}
