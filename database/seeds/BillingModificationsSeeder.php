<?php

use App\Models\Billing\BillingModification;
use Illuminate\Database\Seeder;

class BillingModificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BillingModification::query()
            ->updateOrCreate(['id' => BillingModification::MODIFICATION_PERCENT], [
                'title' => 'Установить процент',
            ])
            ->updateOrCreate(['id' => BillingModification::MODIFICATION_VALUE], [
                'title' => 'Изменить на значение',
            ])
            ->updateOrCreate(['id' => BillingModification::MODIFICATION_MAXIMUM], [
                'title' => 'Ограничить максимальное значение',
            ]);
    }
}
