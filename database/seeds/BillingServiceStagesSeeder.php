<?php

use App\Models\Billing\BillingService;
use App\Models\Billing\BillingServiceStage;
use App\Models\Rent\RentStage;
use Illuminate\Database\Seeder;

class BillingServiceStagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BillingServiceStage::query()
            ->updateOrCreate(['id' => BillingServiceStage::SERVICE_STAGE_BOOK_DURATION], [
                'rent_stage_id'      => RentStage::STAGE_BOOK,
                'billing_service_id' => BillingService::SERVICE_DURATION,
            ])
            ->updateOrCreate(['id' => BillingServiceStage::SERVICE_STAGE_CHECK_DURATION], [
                'rent_stage_id'      => RentStage::STAGE_CHECK,
                'billing_service_id' => BillingService::SERVICE_DURATION,
            ])
            ->updateOrCreate(['id' => BillingServiceStage::SERVICE_STAGE_DRIVE_DURATION], [
                'rent_stage_id'      => RentStage::STAGE_DRIVE,
                'billing_service_id' => BillingService::SERVICE_DURATION,
            ])
            ->updateOrCreate(['id' => BillingServiceStage::SERVICE_STAGE_DRIVE_DISTANCE], [
                'rent_stage_id'      => RentStage::STAGE_DRIVE,
                'billing_service_id' => BillingService::SERVICE_DISTANCE,
            ])
            ->updateOrCreate(['id' => BillingServiceStage::SERVICE_STAGE_PARKING_DURATION], [
                'rent_stage_id'      => RentStage::STAGE_PARKING,
                'billing_service_id' => BillingService::SERVICE_DURATION,
            ]);
    }
}
