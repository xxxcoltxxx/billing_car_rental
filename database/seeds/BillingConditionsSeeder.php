<?php

use App\Models\Billing\BillingCondition;
use Illuminate\Database\Seeder;

class BillingConditionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BillingCondition::query()
            ->updateOrCreate(['id' => BillingCondition::CONDITION_LESS], [
                'title' => 'Значение менее',
            ])
            ->updateOrCreate(['id' => BillingCondition::CONDITION_GREATER], [
                'title' => 'Значение более',
            ])
            ->updateOrCreate(['id' => BillingCondition::CONDITION_TIME], [
                'title' => 'Значение находится во временном периоде',
            ]);
    }
}
