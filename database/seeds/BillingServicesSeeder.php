<?php

use App\Models\Billing\BillingService;
use Illuminate\Database\Seeder;

class BillingServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BillingService::query()
            ->updateOrCreate(['id' => BillingService::SERVICE_DURATION], [
                'title' => 'Поминутная оплата',
            ])
            ->updateOrCreate(['id' => BillingService::SERVICE_DISTANCE], [
                'title' => 'Оплата за км.',
            ]);
    }
}
