<?php

use App\Models\Billing\BillingCondition;
use App\Models\Billing\BillingModification;
use App\Models\Billing\BillingPlan;
use App\Models\Billing\BillingPlanService;
use App\Models\Billing\BillingServiceStage;
use Illuminate\Database\Seeder;

class ExampleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var BillingPlan $plan */
        $plan = factory(BillingPlan::class)->create([
            'title' => 'Новогодний подарок',
        ]);

        /** @var BillingPlanService $service */
        $service = $plan->billingPlanServices()->create([
            'billing_service_stage_id' => BillingServiceStage::SERVICE_STAGE_BOOK_DURATION,
            'price' => 2.00,
        ]);

        $service->billingPlanServiceRules()->create([
            'title' => 'Первые 20 минут - бесплатно',
            'billing_condition_id' => BillingCondition::CONDITION_LESS,
            'condition_parameters' => ['value' => 20],
            'billing_modification_id' => BillingModification::MODIFICATION_PERCENT,
            'modification_amount' => 0.00,
        ]);

        $service->billingPlanServiceRules()->create([
            'title' => 'С 23:00 - 7:00 бронирование бесплатно',
            'billing_condition_id' => BillingCondition::CONDITION_TIME,
            'condition_parameters' => ['from' => '23:00:00', 'to' => '07:00:00'],
            'billing_modification_id' => BillingModification::MODIFICATION_PERCENT,
            'modification_amount' => 0.00,
        ]);

        $service = $plan->billingPlanServices()->create([
            'billing_service_stage_id' => BillingServiceStage::SERVICE_STAGE_CHECK_DURATION,
            'price' => 2.00,
        ]);

        $service->billingPlanServiceRules()->create([
            'title' => 'первые 7 минут - бесплатно',
            'billing_condition_id' => BillingCondition::CONDITION_LESS,
            'condition_parameters' => ['value' => 20],
            'billing_modification_id' => BillingModification::MODIFICATION_PERCENT,
            'modification_amount' => 0.00,
        ]);
    }
}
