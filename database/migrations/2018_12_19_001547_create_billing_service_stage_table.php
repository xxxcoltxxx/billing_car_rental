<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingServiceStageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_service_stages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('billing_service_id')->index();
            $table->integer('rent_stage_id')->index();

            $table->foreign('billing_service_id')->references('id')->on('billing_services');
            $table->foreign('rent_stage_id')->references('id')->on('rent_stages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_service_stage');
    }
}
