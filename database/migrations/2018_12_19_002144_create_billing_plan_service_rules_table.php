<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingPlanServiceRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_plan_service_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('billing_plan_service_id')->index();
            $table->integer('billing_condition_id')->index();
            $table->integer('billing_modification_id')->index();
            $table->integer('car_class_id')->index()->nullable();
            $table->string('title');
            $table->jsonb('modification_amount');
            $table->jsonb('condition_parameters');
            $table->timestamps();

            $table->foreign('billing_plan_service_id')->references('id')->on('billing_plan_services');
            $table->foreign('billing_condition_id')->references('id')->on('billing_conditions');
            $table->foreign('billing_modification_id')->references('id')->on('billing_modifications');
            $table->foreign('car_class_id')->references('id')->on('car_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_plan_service_rules');
    }
}
