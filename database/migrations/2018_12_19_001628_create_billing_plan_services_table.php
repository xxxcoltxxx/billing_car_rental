<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingPlanServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_plan_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('billing_plan_id')->index();
            $table->integer('billing_service_stage_id')->index();
            $table->decimal('price');
            $table->timestamps();

            $table->unique(['billing_plan_id', 'billing_service_stage_id']);
            $table->foreign('billing_plan_id')->references('id')->on('billing_plans');
            $table->foreign('billing_service_stage_id')->references('id')->on('billing_service_stages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_plan_services');
    }
}
